<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

		</div><!-- #content -->
	</div><!-- .site-content-contain -->

    <?php if (is_active_sidebar('primary')): ?>
        <aside id="primary-sidebar" class="primary-sidebar">
            <?php dynamic_sidebar('primary'); ?>
        </aside>
    <?php endif; ?>

    <?php if (is_active_sidebar('footer')): ?>
        <aside id="footer-sidebar" class="footer-sidebar">
            <?php dynamic_sidebar('footer'); ?>
        </aside>
    <?php endif; ?>
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
