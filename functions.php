<?php
/**
 * Equinox functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Equinox
 * @since 1.0
 */

namespace net\icewindow\equinox;

function setup() {

	load_theme_textdomain( 'equinox', get_stylesheet_directory() . '/language' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 175,
		'height'      => 175,
		'flex-width'  => true,
		'flex-height' => true
	) );
}

add_action( 'after_setup_theme', 'net\icewindow\equinox\setup' );

function registerSidebars() {
	register_sidebar( array(
		'id'          => 'primary',
		'name'        => __( 'Primary', 'equinox' ),
		'description' => __( 'This sidebar will be displayed opposite of the main menu.', 'equinox' ),
        'before_widget' => '<div id="%1$s" class="equinox-widget %2$s">',
        'after_widget'  => '</div>'
	) );
	register_sidebar( array(
		'id'          => 'footer',
		'name'        => __( 'Footer sidebar', 'equinox' ),
		'description' => __( 'This sidebar will be displayed on the bottom of the page, outside the regular content area.', 'equinox' ),
        'before_widget' => '<div id="%1$s" class="equinox-widget %2$s">',
        'after_widget'  => '</div>'
	) );
}

add_action( 'widgets_init', 'net\icewindow\equinox\registerSidebars' );

function registerNavMenu() {
	register_nav_menu( 'main-menu', __( 'Main menu', 'equinox' ) );
}

add_action( 'init', 'net\icewindow\equinox\registerNavMenu' );

function menuAttributes( $attributes, $item, $args ) {
	if ( isset( $attributes['class'] ) ) {
		$attributes['class'] .= " menu-link";
	} else {
		$attributes['class'] = "menu-link";
	}

	return $attributes;
}

add_filter( 'nav_menu_link_attributes', 'net\icewindow\equinox\menuAttributes', 10, 3 );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function javascriptDetection() {
	echo "<script type='text/javascript'>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action( 'wp_head', 'net\icewindow\equinox\javascriptDetection', 0 );

/**
 * Enqueue scripts and styles.
 */
function enqueueScripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'equinox-style', get_stylesheet_uri() );
	wp_enqueue_style( 'equinox-font-awesome', get_theme_file_uri( '/assets/css/font-awesome.css' ), array( 'equinox-style' ), '1.0' );

	// Theme's main Javascript
	wp_enqueue_script( 'equinox', get_theme_file_uri( '/assets/js/equinox.js' ), array( 'jquery' ), '1.0', true );

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'equinox-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'equinox-style' ), '1.0' );
		wp_style_add_data( 'equinox-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'equinox-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'equinox-style' ), '1.0' );
	wp_style_add_data( 'equinox-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );
}

add_action( 'wp_enqueue_scripts', 'net\icewindow\equinox\enqueueScripts' );

function styleInit() {
    function hex2rgb($hex) {
        $color = intval(ltrim($hex, '#'), 16);
        $red   = $color >> 16;
        $green = ($color >> 8) % 256;
        $blue  = $color % 256;
        echo "{$red}, {$green}, {$blue}";
    }

	$default = get_option( 'equinox_default_post_background' );
	if ( is_numeric( $default ) ) {
		$default = esc_url(wp_get_attachment_image_url($default, 'full'));
	} else {
		$default = esc_url(get_theme_file_uri('/assets/images/Equinox.png'));
	}
	$contentColor = get_option('equinox_color_content', '#ffffff');
	$menuColor    = get_option('equinox_color_menu', '#c0c0c0');

	?>
    <!-- Style customizations -->
    <style type="text/css" id="equinox-custom-styles">
        <?php if (!empty($default)):?>
        #pagebg {
            background-image: url(<?php echo esc_url( $default); ?>);
        }

        <?php endif; ?>
        .site-content-contain {
            background-color: rgba(<?php hex2rgb($contentColor); ?>, <?php echo get_option( 'equinox_opacity_content', 55) / 100; ?>);
        }

        .equinox-widget {
            background-color: rgba(<?php hex2rgb($menuColor); ?>, <?php echo get_option( 'equinox_opacity_menu', 40) / 100; ?>);
        }

        @media screen and (max-width: 767px) {
            .equinox-menu-container {
                background-color: rgba(<?php hex2rgb($menuColor); ?>, <?php echo get_option( 'equinox_opacity_menu', 40) / 100; ?>);
            }

            #equinox-sidebar-opener:checked ~ .equinox-menu-container,
            #equinox-menu-opener:checked ~ .equinox-menu-container {
                background-color: rgba(<?php hex2rgb($menuColor); ?>, 1);
            }

            #equinox-sidebar-opener:checked ~ .primary-sidebar {
                background-color: rgba(<?php hex2rgb($menuColor); ?>, 1);
            }
        }
    </style>
<?php }

add_action( 'wp_head', 'net\icewindow\equinox\styleInit' );

function scriptInit() { ?>
    <script type="text/javascript">
        if ("undefined" == typeof Equinox) {
            console.error("Equinox doesn't seem to be loaded, can't continue");
        } else {
            Equinox.ajaxUrl = "<?php echo get_template_directory_uri() . "/equinox-ajax.php";?>";
            Equinox.ajaxBlacklist = ["wp-admin/.*", "wp-login\.php"];
            Equinox.init();
        }
    </script>
<?php }

add_action( 'wp_footer', 'net\icewindow\equinox\scriptInit', 1000 );

/**
 * @param string[] $classes An array of body class names.
 * @param string[] $class   An array of additional class names added to the body.
 *
 * @return string[] Array of classes to add to the body
 */
function bodyClass( $classes, $class ) {
    if (is_active_sidebar('primary')) {
        $classes[] = 'has-sidebar';
        $classes[] = 'sidebar-primary';
    }
    if (is_active_sidebar('footer')) {
        $classes[] = 'has-sidebar';
        $classes[] = 'sidebar-footer';
    }
    return $classes;
}

add_filter( 'body_class', 'net\icewindow\equinox\bodyClass', 10, 2 );

// Include Equinox admin-panel specific functions
require get_parent_theme_file_path( '/inc/equinox-admin.php' );

require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );
