<?php
/**
 * Created by PhpStorm.
 * User: icewindow
 * Date: 13.02.17
 * Time: 16:36
 */

define( 'DOING_AJAX', true );
define('EQUINOX_AJAX', true);

function equinox_ajax_headers( $headers, $wp ) {
	$headers['Content-Type'] = 'application/json; Charset:UTF-8';

	return $headers;
}

/** Load WordPress Bootstrap */
require_once( dirname( __FILE__ ) . '/../../../wp-load.php' );

$_urlParts = parse_url(home_url());
$_hostUrl = "{$_urlParts['scheme']}://{$_urlParts['host']}";
if (isset($_urlParts['port'])) $_hostUrl .= ":{$_urlParts['port']}";
$_SERVER['REQUEST_URI'] = str_replace( $_hostUrl, '', $_POST['url'] );
unset($_urlParts, $_hostUrl);

add_filter( 'wp_headers', 'equinox_ajax_headers', 10, 2 );

wp();

/** Allow for cross-domain requests (from the front end). */
send_origin_headers();

// @formatter:off
$template = false;
if     ( is_embed()             && $template = get_embed_template()             ) :
elseif ( is_404()               && $template = get_404_template()               ) :
elseif ( is_search()            && $template = get_search_template()            ) :
elseif ( is_front_page()        && $template = get_front_page_template()        ) :
elseif ( is_home()              && $template = get_home_template()              ) :
elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
elseif ( is_tax()               && $template = get_taxonomy_template()          ) :
elseif ( is_attachment()        && $template = get_attachment_template()        ) :
	remove_filter('the_content', 'prepend_attachment');
elseif ( is_single()            && $template = get_single_template()            ) :
elseif ( is_page()              && $template = get_page_template()              ) :
elseif ( is_singular()          && $template = get_singular_template()          ) :
elseif ( is_category()          && $template = get_category_template()          ) :
elseif ( is_tag()               && $template = get_tag_template()               ) :
elseif ( is_author()            && $template = get_author_template()            ) :
elseif ( is_date()              && $template = get_date_template()              ) :
elseif ( is_archive()           && $template = get_archive_template()           ) :
else :
	$template = get_index_template();
endif;
// @formatter:on

$fragment = str_replace( ".php", "", basename( $template ) );

do_action('equinox_before_ajax_content');

ob_start();
get_template_part( "template-parts/fragments/fragment", $fragment );
$content = ob_get_clean();

do_action('equinox_after_ajax_content');

$title = wp_title( '–', false, 'right' ) . get_bloginfo( 'name' );

$background = get_post_meta( $post->ID, 'equinox_post_background', true );
if ( empty( $background ) ) {
	$background = false;
} else {
	if ( is_numeric( $background ) ) {
		$background = esc_url(wp_get_attachment_image_url($background, 'full'));
	} else {
		$background = esc_url(get_theme_file_uri('/assets/images/Equinox.png'));
	}
}

echo( json_encode( array(
	'title'      => $title,
	'content'    => $content,
	'post_id'    => $post->ID,
	'background' => $background
) ) );

wp_die();
