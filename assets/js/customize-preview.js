/**
 * File customize-preview.js.
 *
 * Instantly live-update customizer settings in the preview for improved user experience.
 */

(function ($) {

    var values = {
        equinox_color_content:   null,
        equinox_opacity_content: null,
        equinox_color_menu:      null,
        equinox_opacity_menu:    null
    };

    // Collect information from customize-controls.js about which panels are opening.
    wp.customize.bind('preview-ready', function () {

        // Initially hide the theme option placeholders on load
        $('.panel-placeholder').hide();

        wp.customize.preview.bind('section-highlight', function (data) {

            // Only on the front page.
            if (!$('body').hasClass('twentyseventeen-front-page')) {
                return;
            }

            // When the section is expanded, show and scroll to the content placeholders, exposing the edit links.
            if (true === data.expanded) {
                $('body').addClass('highlight-front-sections');
                $('.panel-placeholder').slideDown(200, function () {
                    $.scrollTo($('#panel1'), {
                        duration: 600,
                        offset: {'top': -70} // Account for sticky menu.
                    });
                });

                // If we've left the panel, hide the placeholders and scroll back to the top.
            } else {
                $('body').removeClass('highlight-front-sections');
                // Don't change scroll when leaving - it's likely to have unintended consequences.
                $('.panel-placeholder').slideUp(200);
            }
        });

        values = {
            equinox_color_content:   wp.customize.settings.values.equinox_color_content,
            equinox_opacity_content: wp.customize.settings.values.equinox_opacity_content,
            equinox_color_menu:      wp.customize.settings.values.equinox_color_menu,
            equinox_opacity_menu:    wp.customize.settings.values.equinox_opacity_menu
        };
    });

    // Site title and description.
    wp.customize('blogname', function (value) {
        value.bind(function (to) {
            $('.site-title a').text(to);
        });
    });
    wp.customize('blogdescription', function (value) {
        value.bind(function (to) {
            $('.site-description').text(to);
        });
    });

    // Header text color.
    wp.customize('header_textcolor', function (value) {
        value.bind(function (to) {
            if ('blank' === to) {
                $('.site-title, .site-description').css({
                    clip: 'rect(1px, 1px, 1px, 1px)',
                    position: 'absolute'
                });
                // Add class for different logo styles if title and description are hidden.
                $('body').addClass('title-tagline-hidden');
            } else {

                // Check if the text color has been removed and use default colors in theme stylesheet.
                if (!to.length) {
                    $('#twentyseventeen-custom-header-styles').remove();
                }
                $('.site-title, .site-description').css({
                    clip: 'auto',
                    position: 'relative'
                });
                $('.site-branding, .site-branding a, .site-description, .site-description a').css({
                    color: to
                });
                // Add class for different logo styles if title and description are visible.
                $('body').removeClass('title-tagline-hidden');
            }
        });
    });

    // Color scheme.
    function _hex2rgb(hex) {
        // Source https://stackoverflow.com/a/5624139
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    function _setBackgroundColor(element, red, green, blue, opacity) {
        // Make sure all values are in bounds
        red       = Math.min(255, Math.max(0, red));
        green     = Math.min(255, Math.max(0, green));
        blue      = Math.min(255, Math.max(0, blue));
        opacity   = Math.min(1,   Math.max(0, opacity));
        var sheet = document.getElementById('equinox-custom-styles').sheet;
        sheet.insertRule(`.${element} { background-color: rgba(${red}, ${green}, ${blue}, ${opacity}); }`, sheet.rules.length);
    }

    function contentColor() {
        var opacity = parseInt(values.equinox_opacity_content) / 100;
        var rgb = _hex2rgb(values.equinox_color_content);
        _setBackgroundColor('site-content-contain', rgb.r, rgb.g, rgb.b, opacity);
    }

    function menuColor() {
        var opacity = parseInt(values.equinox_opacity_menu) / 100;
        var rgb = _hex2rgb(values.equinox_color_menu);
        _setBackgroundColor('equinox-widget', rgb.r, rgb.g, rgb.b, opacity);
    }

    $.each(values, function(index, element) {
        wp.customize(index, (val) => val.bind((to) => values[index] = to));
    });

    wp.customize('equinox_color_content',   (val) => val.bind(contentColor));
    wp.customize('equinox_opacity_content', (val) => val.bind(contentColor));
    wp.customize('equinox_color_menu',      (val) => val.bind(menuColor));
    wp.customize('equinox_opacity_menu',    (val) => val.bind(menuColor));

    // Page layouts.
    wp.customize('page_layout', function (value) {
        value.bind(function (to) {
            if ('one-column' === to) {
                $('body').addClass('page-one-column').removeClass('page-two-column');
            } else {
                $('body').removeClass('page-one-column').addClass('page-two-column');
            }
        });
    });

    // Whether a header image is available.
    function hasHeaderImage() {
        var image = wp.customize('header_image')();
        return '' !== image && 'remove-header' !== image;
    }

    // Whether a header video is available.
    function hasHeaderVideo() {
        var externalVideo = wp.customize('external_header_video')(),
            video = wp.customize('header_video')();

        return '' !== externalVideo || ( 0 !== video && '' !== video );
    }

    // Toggle a body class if a custom header exists.
    $.each(['external_header_video', 'header_image', 'header_video'], function (index, settingId) {
        wp.customize(settingId, function (setting) {
            setting.bind(function () {
                if (hasHeaderImage()) {
                    $(document.body).addClass('has-header-image');
                } else {
                    $(document.body).removeClass('has-header-image');
                }

                if (!hasHeaderVideo()) {
                    $(document.body).removeClass('has-header-video');
                }
            });
        });
    });

})(jQuery);
