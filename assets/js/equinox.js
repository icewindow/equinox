/**
 * Created by icewindow on 12.02.17.
 */

var Equinox = new (function () {

    var _eventListeners = [];
    var $ = jQuery.noConflict();

    this.ajaxUrl = null;
    this.ajaxBlacklist = null;

    var $menu, $content, $contentContainer, $menuOpener, $sidebar;

    this.init = function () {
        $menu = $('#menu');
        $content = $('#content');
        $contentContainer = $('#content-container');
        $menuOpener = $('#equinox-menu-opener');
        $sidebar = $('#primary-sidebar');
        this._registerListeners();
        $menu.data("menuInterval", setInterval(this._avoidMenuCollision.bind(this), 100));
        if ($sidebar.length) $sidebar.data("sidebarInterval", setInterval(this._avoidSidebarCollision.bind(this), 100));
        window.history.replaceState({url: window.location.href}, "");
    };

    this.loadPage = function (url, noHistoryPush) {
        $contentContainer.fadeOut(function () {
            if (!noHistoryPush) {
                window.history.pushState({url: url}, "", url);
            }
            $.ajax({
                data: {
                    url: url
                },
                method: 'post',
                url: Equinox.ajaxUrl,
                dataType: 'json',
                success: function (response) {
                    if (response.error) {
                        alert(response.error);
                        return;
                    }
                    document.title = response.title;
                    $content.html(response.content);
                    $contentContainer.fadeIn();
                    if ($(document.body).hasClass('admin-bar')) {
                        var admin = $('#wpadminbar');
                        $('#wp-admin-bar-edit').find('a').prop('href', "/wp-admin/post.php?post=" + response.post_id + "&action=edit");
                        $('#wp-admin-bar-customize').find('a').prop('href', '/wp-admin/customize.php?url=' + encodeURIComponent(window.location));
                    }
                    var $replacebg = $('#replacebg');
                    var $fadebg = $('#fadebg');
                    if (response.background) {
                        $fadebg.hide();
                        var image = new Image();
                        image.onload = function() {
                            $fadebg.css('background-image', 'url(' + response.background + ')');
                            $fadebg.fadeIn(400, function () {
                                $replacebg.css('background-image', 'url(' + response.background + ')');
                                $fadebg.css('background-image', '');
                                $fadebg.hide();
                            });
                        };
                        image.src = response.background;
                    } else {
                        $replacebg.fadeOut(400, function() {
                            $replacebg.css('background-image', '');
                            $replacebg.show();
                        });
                    }
                    Equinox.dispatchEvent({
                        type: 'ajaxPageLoaded',
                        pageTitle: response.title,
                        pageID: response.post_id,
                        content: response.content,
                        background: response.background
                    });
                }
            });
        });
    };

    // Private functions

    this._registerListeners = function() {
        $(window).on('blur focus', function (event) {
            if ($(this).data("previousType") != event.type) {
                Equinox.dispatchEvent(new Event("window" + event.type));
            }
            $(this).data("previousType", event.type);
        }).on('popstate', function(event) {
            Equinox.loadPage(event.originalEvent.state.url, true);
        });

        $menu.on('mouseover', () => {
            $menu.data("menu-paused", true).data("menu-timer", 0);
            this._showMenu();
        }).on('mouseout', function () {
            $menu.data("menu-paused", false);
        }).find('.menu-hide p').on('click', this._hideMenu);

        $sidebar.on('mouseover', () => {
            $sidebar.data("menu-paused", true).data("menu-timer", 0);
            this._showSidebar();
        }).on('mouseout', function () {
            $sidebar.data("menu-paused", false);
        }).find('.menu-hide p').on('click', this._hideSidebar);

        $(document).on('click', (event) => {
            if ("A" != event.target.tagName) return;
            var $this = $(event.target);
            var url = $this.prop("href");
            $menuOpener.prop('checked', false);  // Close the menu. If the menu is already closed or not in responsive mode, this will effectively do nothing
            if (!this._isAjaxBlacklisted(url)) {
                event.preventDefault();
                if (!this._isIgnoredLink(url)) {
                    if ($this.hasClass('menu-link')) {
                        $menu.find('li').removeClass("current-menu-item current-menu-ancestor current-menu-parent");
                        var $parent = $this.parent();
                        $parent.addClass("current-menu-item");
                        do {
                            $parent = $parent.parent();
                            if ("LI" == $parent.prop("tagName")) {
                                $parent.addClass("current-menu-ancestor");
                            }
                        } while ("DIV" != $parent.prop("tagName"));
                    }
                    Equinox.loadPage(url);
                }
            }
        });
    };

    this._isIgnoredLink = function(link) {
        var url = window.location.href;
        if (link == url)  return true;  // Ignore the link if it directs to the page we're already on
        var hashPos = link.indexOf('#');
        if (hashPos >= 0) {
            //compare URL and hash if we have both
            var hash = link.substr(hashPos + 1);
            link = link.substr(0, hashPos);
            if (window.location.hash == hash && link == url) return true;
        }
        return false;
    };

    this._isAjaxBlacklisted = function(link) {
        var linkParts = link.split(/:?\/+/g);
        if (window.location.host != linkParts[1]) return true;  // Blacklist every link that's not local
        var path = link.substr(link.indexOf("/", window.location.protocol.length + 3) + 1);
        for (var i = 0; i < Equinox.ajaxBlacklist.length; i++) {
            if (path.match(new RegExp(Equinox.ajaxBlacklist[i]))) return true;
        }
        return false;
    };

    // Menu collision

    this._hideMenu = function() {
        $menu.data("menu-hidden", true).css("left", -1 * ($menu.outerWidth() - 15)).addClass("equinox-hidden");
    };

    this._showMenu = function() {
        $menu.data("menu-hiden", false).css("left", "").removeClass("equinox-hidden");
    };

    this._detectMenuCollision = function() {
        var rightEdge = $menu.offset().left + $menu.outerWidth();
        return ($content.offset().left - parseInt($content.css('left'))) < rightEdge;
    };

    this._avoidMenuCollision = function() {
        if (window.innerWidth >= 768 && !$menu.data("menu-paused") && this._detectMenuCollision()) {
            var timer = $menu.data("menu-timer") || 0;
            if (timer > 50) {
                this._hideMenu();
            }
            $menu.data("menu-timer", ++timer);
        }
    };

    // sidebar collision

    this._hideSidebar = function() {
        $sidebar.data("menu-hidden", true).css("right", -1 * ($sidebar.outerWidth() - 15)).addClass("equinox-hidden");
    };

    this._showSidebar = function() {
        $sidebar.data("menu-hiden", false).css("right", "").removeClass("equinox-hidden");
    };

    this._detectSidebarCollision = function() {
        var rightEdge = $sidebar.offset().left + $sidebar.outerWidth();
        return ($content.offset().left - parseInt($content.css('left'))) < rightEdge;
    };

    this._avoidSidebarCollision = function() {
        if (window.innerWidth >= 768 && !$sidebar.data("menu-paused") && this._detectSidebarCollision()) {
            var timer = $sidebar.data("menu-timer") || 0;
            if (timer > 50) {
                this._hideSidebar();
            }
            $sidebar.data("menu-timer", ++timer);
        }
    };

    // Event handling

    this.addEventListener = function (type, listener) {
        if ("undefined" == typeof _eventListeners[type]) _eventListeners[type] = [];
        _eventListeners[type].push(listener);
    };

    this.removeEventListener = function (type, listener) {
        if (_eventListeners[type] instanceof Array) {
            for (var i = 0; i < _eventListeners[type].length; i++) {
                if (_eventListeners[type][i] === listener) {
                    _eventListeners[type].splice(i, 1);
                    break;
                }
            }
        }
    };

    this.dispatchEvent = function (event) {
        if (!event.target) event.target = this;
        if (_eventListeners[event.type] instanceof Array) {
            for (var i = 0; i < _eventListeners[event.type].length; i++) {
                _eventListeners[event.type][i].call(this, event);
            }
        }
    };

})();