/**
 * Created by icewindow on 26.02.17.
 */

var EquinoxAdmin = new (function ($) {

    $(function () {
        $('#equinox-open-media-selector').on('click', function (event) {
            if (wp.media.frames.equinox) {
                wp.media.frames.equinox.open();
                return;
            }
            // configuration of the media manager new instance
            wp.media.frames.equinox = wp.media({
                title: EquinoxAdminL10n.select_images,
                multiple: true,
                library: {
                    type: 'image'
                },
                button: {
                    text: EquinoxAdminL10n.use_images
                }
            });

            function equinoxBackgroundSelected() {
                var selection = wp.media.frames.equinox.state().get('selection');

                // no selection
                if (!selection) {
                    return;
                }

                var ids = [];
                var selectedImage = false;
                var currentlySelected = $('input[name=equinox_default_post_background]:checked').val();

                var $container = $('#equinox-post-backgrounds-container');
                var $default = $container.find('div').eq(0);
                $container.empty().append($default);

                // iterate through selected elements
                selection.each(function (attachment) {
                    ids.push(attachment.attributes.id);

                    var $div = $('<div>' +
                        '<input type="radio" id="equiox-post-background-' + attachment.attributes.id + '" name="equinox_default_post_background" value="' + attachment.attributes.id + '"/>' +
                        '<label for="equiox-post-background-' + attachment.attributes.id + '">' +
                        '<img src="' + attachment.attributes.url + '" alt="' + attachment.attributes.title + '"/>' +
                        '</label>' +
                        '</div>'
                    );

                    if (currentlySelected == attachment.attributes.id) {
                        $div.find('input').prop('checked', true);
                        selectedImage = true;
                    }
                    $container.append($div);
                });

                if (!selectedImage) {
                    $('#equinox-post-background-default').prop('checked', true);
                }
                $('#equinox-post-backgrounds').val(ids.join(','));
            }

            // image selection event
            wp.media.frames.equinox.on('select', equinoxBackgroundSelected);

            wp.media.frames.equinox.on('open', function () {
                var selection = wp.media.frames.equinox.state().get('selection');
                var ids = jQuery('#equinox-post-backgrounds').val().split(',');
                ids.forEach(function (id) {
                    attachment = wp.media.attachment(id);
                    attachment.fetch();
                    selection.add(attachment ? [attachment] : []);
                });
            });

            // showing media manager
            wp.media.frames.equinox.open();
        });
    });

})(jQuery);