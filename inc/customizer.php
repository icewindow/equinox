<?php
/**
 * Equinox: Customizer
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function twentyseventeen_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector'        => '.site-title a',
		'render_callback' => 'twentyseventeen_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector'        => '.site-description',
		'render_callback' => 'twentyseventeen_customize_partial_blogdescription',
	) );

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting( 'equinox_color_content', array(
        'default'           => '#ffffff',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color',
        'type'              => 'option'
    ) );

	$wp_customize->add_setting( 'equinox_opacity_content', array(
		'default'           => 55,
		'transport'         => 'postMessage',
		'sanitize_callback' => 'sanitizePercentage',
		'type'              => 'option'
	) );

	$wp_customize->add_setting( 'equinox_color_menu', array(
	    'default'           => '#c0c0c0',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color',
        'type'              => 'option'
    ) );

	$wp_customize->add_setting( 'equinox_opacity_menu', array(
		'default'           => 40,
		'transport'         => 'postMessage',
		'sanitize_callback' => 'sanitizePercentage',
		'type'              => 'option'
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'equinox_color_content', array(
	    'label'    => __( 'Background color content', 'equinox' ),
        'section'  => 'colors',
        'settings' => 'equinox_color_content',
        'priority' => 5
    ) ) );

	$wp_customize->add_control( 'equinox_opacity_content', array(
		'type'     => 'number',
		'label'    => __( 'Background opacity content', 'equinox' ),
		'section'  => 'colors',
		'settings' => 'equinox_opacity_content',
		'priority' => 5
	) );

    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'equinox_color_menu', array(
        'label'    => __( 'Background color menu and widgets', 'equinox' ),
        'section'  => 'colors',
        'settings' => 'equinox_color_menu',
        'priority' => 5
    ) ) );

	$wp_customize->add_control( 'equinox_opacity_menu', array(
		'type'     => 'number',
		'label'    => __( 'Background opacity menu and widgets', 'equinox' ),
		'section'  => 'colors',
		'settings' => 'equinox_opacity_menu',
		'priority' => 5
	) );
}

add_action( 'customize_register', 'twentyseventeen_customize_register' );


/**
 * Sanitize the colorscheme.
 */
function sanitizePercentage( $input ) {
	$input = absint( $input );
	if ( $input > 100 ) {
		return 100;
	}

	return $input;
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function twentyseventeen_customize_preview_js() {
	wp_enqueue_script( 'twentyseventeen-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview' ), '1.0', true );
}

add_action( 'customize_preview_init', 'twentyseventeen_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function twentyseventeen_panels_js() {
	wp_enqueue_script( 'twentyseventeen-customize-controls', get_theme_file_uri( '/assets/js/customize-controls.js' ), array(), '1.0', true );
}

add_action( 'customize_controls_enqueue_scripts', 'twentyseventeen_panels_js' );
