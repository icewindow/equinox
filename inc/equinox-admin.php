<?php
/**
 * Created by PhpStorm.
 * User: icewindow
 * Date: 24.02.17
 * Time: 13:01
 */

namespace net\icewindow\equinox;

function registerSettings() {
	add_settings_section( 'equinox-backgrounds', __( 'Post backgrounds', 'equinox' ), null, 'equinox-backgrounds' );

	register_setting( 'equinox-backgrounds', 'equinox_post_backgrounds' );
	register_setting( 'equinox-backgrounds', 'equinox_default_post_background' );
}

add_action( 'admin_init', 'net\icewindow\equinox\registerSettings' );

function enqueueAdminScripts() {
	wp_enqueue_script( 'equinox-admin', get_theme_file_uri('/assets/js/equinox-admin.js'));
	wp_localize_script( 'equinox-admin', 'EquinoxAdminL10n', array(
        'select_images' => __('Select image', 'equinox'),
		'use_images' => __( 'Use selected image(s)', 'equinox' )
	) );

	wp_enqueue_style( 'equinox-admin', get_theme_file_uri('/assets/css/equinox-admin.css'));
}

add_action( 'admin_enqueue_scripts', 'net\icewindow\equinox\enqueueAdminScripts' );

function adminPage() {
	if ( ! current_user_can( 'manage_options' ) ) {
		echo "Nothing to see here! Move along.";

		return;
	}

}

function backgroundsPage() {
	if ( ! current_user_can( 'manage_options' ) ) {
		echo "Nothing to see here! Move along.";

		return;
	}
	wp_enqueue_media();
	$selected    = get_option( 'equinox_default_post_background', 'equinox' );
	$backgrounds = get_option( 'equinox_post_backgrounds' );
	$args        = array(
		'include'        => explode( ',', $backgrounds ),
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'posts_per_page' => - 1,
	);
	$available   = get_posts( $args );
	?>
    <form action="options.php" method="post">
		<?php
		settings_fields( 'equinox-backgrounds' );
		do_settings_sections( 'equinox-backgrounds' );
		?>
        <input type="hidden" name="equinox_post_backgrounds" id="equinox-post-backgrounds" value="<?php echo $backgrounds ?>"/>
        <p><?php _e( 'Click the "Select images" button down below to chose which images you want to use as post backgrounds.', 'equinox' ); ?></p>
        <p><?php _e( 'Select the default post background by clicking one of your selected images. The active default background is has a blue border around it.', 'equinox' ); ?></p>
        <div class="equinox-post-background big" id="equinox-post-backgrounds-container">
            <div>
                <input type="radio" <?php checked( $selected, 'equinox' ); ?> id="equinox-post-background-default"
                       name="equinox_default_post_background"
                       value="equinox"/>
                <label for="equinox-post-background-default">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/Equinox.png'; ?>"
                         alt="<?php _e( 'Default background', 'equinox' ); ?>"/>
                </label>
            </div>
			<?php foreach ( $available as $image ) : ?>
                <div>
                    <input type="radio" <?php checked( $selected, $image->ID ); ?>
                           id="equiox-post-background-<?php echo esc_attr( $image->ID ); ?>"
                           name="equinox_default_post_background" value="<?php echo esc_attr( $image->ID ); ?>"/>
                    <label for="equiox-post-background-<?php echo esc_attr( $image->ID ); ?>">
                        <img src="<?php echo esc_url( $image->guid ); ?>" alt="<?php echo esc_attr( $image->post_title ); ?>"/>
                    </label>
                </div>
			<?php endforeach; ?>
        </div>
        <input type="button" class="button button-primary" id="equinox-open-media-selector" value="<?php _e( 'Select images', 'equinox' ); ?>"/>
		<?php submit_button(); ?>
    </form>
	<?php
}

function registerMenus() {
	add_menu_page(
		'equinox',
		__( 'Equinox', 'equinox' ),
		'manage_options',
		'equinox',
		'net\icewindow\equinox\adminPage',
		get_theme_file_uri('/assets/images/equinox_menu_icon.png'),
		90
	);

	add_submenu_page(
		'equinox',
		__( 'Equinox theme settings', 'equinox' ),
		__( 'Equinox theme settings', 'equinox' ),
		'manage_options',
		'equinox'
	);

	add_submenu_page(
		'equinox',
		__( 'Equinox background gallery', 'equinox' ),
		__( 'Equinox background gallery', 'equinox' ),
		'manage_options',
		'equinox-backgrounds',
		'net\icewindow\equinox\backgroundsPage'
	);
}

add_action( 'admin_menu', 'net\icewindow\equinox\registerMenus' );

function displayPostBackgroundMetaBox( $post ) {
	$selected = get_post_meta( $post->ID, 'equinox_post_background', true );
	$default  = get_option( 'equinox_default_post_background' );
	if ( is_numeric( $default ) ) {
		$default = get_post( $default );
		$default = $default->guid;
	} else {
		$default = get_theme_file_uri('/assets/images/Equinox.png');
	}

	$backgrounds = get_option( 'equinox_post_backgrounds' );
	$args        = array(
		'include'        => explode( ',', $backgrounds ),
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'posts_per_page' => - 1,
	);
	$available   = get_posts( $args );
	?>
    <div class="equinox-post-background">
        <div>
            <input type="radio" <?php checked( $selected, '' ); ?> id="equiox-post-background-default" name="equinox_post_background" value=""/>
            <label for="equiox-post-background-default">
                <img src="<?php echo esc_url( $default ); ?>" alt="<?php _e( 'Default background', 'equinox' ); ?>"/>
            </label>
        </div>
        <div>
            <input type="radio" <?php checked( $selected, 'equinox' ); ?> id="equiox-post-background-equinox" name="equinox_post_background"
                   value="equinox"/>
            <label for="equiox-post-background-equinox">
                <img src="<?php echo esc_url( get_theme_file_uri('/assets/images/Equinox.png')); ?>"
                     alt="<?php _e( 'Default theme background', 'equinox' ); ?>"/>
            </label>
        </div>
		<?php foreach ( $available as $image ) : ?>
            <div>
                <input type="radio" <?php checked( $selected, $image->ID ); ?> id="equiox-post-background-<?php echo esc_attr( $image->ID ); ?>"
                       name="equinox_post_background" value="<?php echo esc_attr( $image->ID ); ?>"/>
                <label for="equiox-post-background-<?php echo esc_attr( $image->ID ); ?>">
                    <img src="<?php echo esc_url( $image->guid ); ?>" alt="<?php echo esc_attr( $image->post_title ); ?>"/>
                </label>
            </div>
		<?php endforeach; ?>
    </div>
    <p><?php printf( wp_kses(
			__( 'Go to <a href="%s">Equinox Background Gallery</a> to manage your post backgrounds.', 'equinox' ),
			array( 'a' => array( 'href' => array() ) )
		), esc_url( admin_url( 'admin.php?page=equinox-backgrounds' ) ) ); ?></p>
	<?php
}

function savePostMeta( $postID ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( empty ( $_POST['equinox_post_background'] ) ) {
		delete_post_meta( $postID, 'equinox_post_background' );
	} else {
		update_post_meta( $postID, 'equinox_post_background', $_POST['equinox_post_background'] );
	}
}

add_action( 'save_post', 'net\icewindow\equinox\savePostMeta' );

function registerMetaBoxes() {
	add_meta_box(
		'equinox-post-background',
		__( 'Post background', 'equinox' ),
		'net\icewindow\equinox\displayPostBackgroundMetaBox',
		null,
		'normal'
	);
}

add_action( 'add_meta_boxes', 'net\icewindow\equinox\registerMetaBoxes' );