��          �       l      l     m  #   �  /   �     �  f   �     `     s     �     �     �  R   �  	     	   #     -     =     N     [  �   i     �     	  �         �  +   �  1   �     *  |   G     �     �     �     �       Y   5     �  
   �     �     �     �     �  �   �     �     �   Background opacity content Background opacity menu and widgets Based on Twenty Seventeen by the WordPress team Clemens "icewindow" Baudisch Click the "Select images" button down below to chose which images you want to use as post backgrounds. Default background Default theme background Equinox Equinox background gallery Equinox theme settings Go to <a href="%s">Equinox Background Gallery</a> to manage your post backgrounds. Hide menu Main menu Post background Post backgrounds Select image Select images Select the default post background by clicking one of your selected images. The active default background is has a blue border around it. Use selected image(s) https://icewindow.net/ Project-Id-Version: Equinox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-08 09:14+0000
PO-Revision-Date: 2017-08-08 09:14+0000
Last-Translator: icewindow <icewindow@icewindow.net>
Language-Team: German
Language: de-DE
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/ Hintergrund Transparenz (Inhalt) Hintergrund Transparenz (Menü und Widgets) Basierend auf Twenty Seventeen vom WordPress Team Clemens "icewindow" Baudisch Klicke "Bilder auswählen" unten um die Bilder auszuwählen, welche du als Hintergrundbilder für Posts verwenden möchtest. Standardhintergrund Standard Post Hintergrundbild Equinox Equinox Hintergrundgalerie Equinox Theme-Einstellungen Gehe zur <a href="%s">Equinox Hintergrundgalerie</a> um deine Hintergründe zu verwalten. Menü ausblenden Hauptmenü Hintergrundbild Post Hintergünde Bilder auswählen Bilder auswählen Wähle das Standard-Hintergrundbild aus, indem du auf eines deiner ausgewählten Bilder klickst. Das aktive Standard-Hintergrundbild ist mit einem blauen Rahmen gekennzeichnet. Bild(er) verwenden https://icewindow.net 