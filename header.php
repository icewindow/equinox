<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Equinox
 * @since 1.0
 * @version 1.0
 */

$default = get_post_meta(get_the_ID(), 'equinox_post_background', true);
if (empty($default)) {
	$default = false;
} else {
	if (is_numeric($default)) {
		$default = esc_url(wp_get_attachment_image_url($default));
	} else {
		$default = esc_url(get_theme_file_uri('/assets/images/Equinox.png'));
	}
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="pagebg" class="background"></div>
<div id="replacebg" class="background" <?php if ($default) echo 'style="background-image: url(' . $default . ');"' ?>></div>
<div id="fadebg" class="background"></div>
<div id="page" class="site">
    <input type="checkbox" class="equinox-menu-opener" id="equinox-menu-opener">
    <input type="checkbox" class="equinox-menu-opener" id="equinox-sidebar-opener">
    <div class="equinox-menu-container" id="menu">
        <div class="equinox-widget equinox-menu">
            <div class="menu-image"><?php
				$custom_logo_id = get_theme_mod('custom_logo');
				$logo           = wp_get_attachment_image_src($custom_logo_id, 'full');
				if (has_custom_logo()) {
					echo '<img src="' . esc_url($logo[0]) . '">';
				}
				?></div>
			<?php wp_nav_menu(array('theme_location' => 'main-menu', 'container' => false, 'menu_id' => 'equinox-menu')); ?>
            <div class="menu-hide"><p><?php _e('Hide menu', 'equinox'); ?></p></div>
        </div>
    </div>
    <div class="equinox-menu-opener">
        <label for="equinox-menu-opener"><span class="burger"><span class="burger-inner"></span></span></label>
    </div>
    <div class="equinox-sidebar-opener">
        <label for="equinox-sidebar-opener"><span class="door"><span class="door-inner"></span></span></label>
    </div>

    <div id="content-container" class="site-content-contain">
        <div id="content" class="site-content">
